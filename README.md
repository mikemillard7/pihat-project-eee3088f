# PiHat Project EEE3088F

This focus of this project is to design, simulate and combine different subsystems of an uninterrupted power supply (UPS) which make will work as a PiHat that connects to a Raspberry Pi (Pi Zero model) and can power a 12V WiFi router in a power cut.

Instructions:
The PiHat consists of 3 subsystems. Two of which are voltage regulator circuits. The first voltage regulator is to have the Raspberry Pi Zero board connected to its output. It provides a regulated 3.3V DC voltage to the Pi with its inherent current draw and power usage characteristics accounted for during the design phase. The same process was followed when designing the second voltage regulator which provides a 12V DC voltage to the WiFi router in the case of a power cut. The third sub system is a switch that detects a cut in the mains and initiates the process of the battery supplies connecting the the voltage regulators, which are then connected to the Pi Zero board and the WiFi router.
